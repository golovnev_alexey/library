<?php

namespace App\Http\Controllers;

use App\Models\Writer;
use App\Responses\ApiResponse;
use Illuminate\Http\Request;

class WriterController extends Controller
{
    public function index(Request $request)
    {
        $writers = Writer::filter($request->all())->get();

        return ApiResponse::json($writers);
    }

    public function show(Request $request, int $id)
    {
        $writer = Writer::query()->findOrFail($id);

        return ApiResponse::json($writer);
    }

    public function store(Request $request)
    {
        $writer = new Writer($request->all());

        $writer->save();

        return ApiResponse::json($writer);
    }

    public function update(Request $request, int $id)
    {
        $writer = Writer::query()->findOrFail($id);

        $writer->update($request->all());

        return ApiResponse::json($writer);
    }

    public function destroy(Request $request, int $id)
    {
        $writer = Writer::query()->findOrFail($id);

        $writer->delete();

        return ApiResponse::json($writer);
    }
}
