<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Responses\ApiResponse;
use Illuminate\Http\Request;

class BookController extends Controller
{
    public function index(Request $request)
    {
        $books = Book::filter($request->all())->get();

        return ApiResponse::json($books);
    }

    public function show(Request $request, int $id)
    {
        $book = Book::query()->findOrFail($id);

        return ApiResponse::json($book);
    }

    public function store(Request $request)
    {
        $book = new Book($request->all());

        $book->save();

        return ApiResponse::json($book);
    }

    public function update(Request $request, int $id)
    {
        $book = Book::query()->findOrFail($id);

        $book->update($request->all());

        return ApiResponse::json($book);
    }

    public function destroy(Request $request, int $id)
    {
        $book = Book::query()->findOrFail($id);

        $book->delete();

        return ApiResponse::json($book);
    }
}
