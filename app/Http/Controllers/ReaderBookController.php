<?php

namespace App\Http\Controllers;

use App\Models\ReaderBook;
use App\Responses\ApiResponse;
use Illuminate\Http\Request;

class ReaderBookController extends Controller
{
    public function index(Request $request)
    {
        $readerBooks = ReaderBook::filter($request->all())->get();

        return ApiResponse::json($readerBooks);
    }

    public function show(Request $request, int $id)
    {
        $readerBook = ReaderBook::query()->findOrFail($id);

        return ApiResponse::json($readerBook);
    }

    public function store(Request $request)
    {
        $readerBook = new ReaderBook($request->all());

        $readerBook->save();

        return ApiResponse::json($readerBook);
    }

    public function update(Request $request, int $id)
    {
        $readerBook = ReaderBook::query()->findOrFail($id);

        $readerBook->update($request->all());

        return ApiResponse::json($readerBook);
    }

    public function destroy(Request $request, int $id)
    {
        $readerBook = ReaderBook::query()->findOrFail($id);

        $readerBook->delete();

        return ApiResponse::json($readerBook);
    }
}
