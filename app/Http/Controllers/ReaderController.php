<?php

namespace App\Http\Controllers;

use App\Models\Reader;
use App\Responses\ApiResponse;
use Illuminate\Http\Request;

class ReaderController extends Controller
{
    public function index(Request $request)
    {
        $readers = Reader::filter($request->all())->get();

        return ApiResponse::json($readers);
    }

    public function show(Request $request, int $id)
    {
        $reader = Reader::query()->findOrFail($id);

        return ApiResponse::json($reader);
    }

    public function store(Request $request)
    {
        $reader = new Reader($request->all());

        $reader->save();

        return ApiResponse::json($reader);
    }

    public function update(Request $request, int $id)
    {
        $reader = Reader::query()->findOrFail($id);

        $reader->update($request->all());

        return ApiResponse::json($reader);
    }

    public function destroy(Request $request, int $id)
    {
        $reader = Reader::query()->findOrFail($id);

        $reader->delete();

        return ApiResponse::json($reader);
    }
}

