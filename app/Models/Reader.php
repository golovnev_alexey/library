<?php

namespace App\Models;

use App\Scopes\ScopeFilterTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reader extends Model
{
    use HasFactory;

    use ScopeFilterTrait;

    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name',
        'address',
        'phone',
    ];
}
