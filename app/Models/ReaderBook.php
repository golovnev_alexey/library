<?php

namespace App\Models;

use App\Scopes\ScopeFilterTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReaderBook extends Model
{
    use HasFactory;

    use ScopeFilterTrait;

    protected $table = 'reader_book';

    protected $fillable = [
        'reader_id',
        'book_id',
        'start_at',
        'end_at',
    ];

    protected $with = [
        'reader',
        'book'
    ];

    public function reader()
    {
        return $this->belongsTo(Reader::class);
    }

    public function book()
    {
        return $this->belongsTo(Book::class);
    }
}
