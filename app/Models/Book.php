<?php

namespace App\Models;

use App\Scopes\ScopeFilterTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;

    use ScopeFilterTrait;

    const GENRES_LIST = [
        'Детектив (криминальный, светский, исторический)',
        'Фантастика (научная фантастика, фентези)',
        'Приключения',
        'Роман (исторический, любовный, приключенческий)',
        'Научная книга (научные труды, статьи)',
        'Фольклор (эпос, легенды, сказки)',
        'Юмор (анекдоты, союз)',
        'Справочная книга',
        'Поэзия, проза',
        'Детская книга',
        'Документальная литература (биографии, публицистика,, военное дело)',
        'Деловая литература (бизнес, политика, финансы, экономика)',
        'Религиозная литература',
        'Учебная книга (методическое пособие, учебник)',
        'Книги о психологии',
        'Хобби, досуг (отдых, туризм, домашние животные, домоводство)',
        'Зарубежная, русская литература',
        'Техника (авто, бытовая техника, компьютер)',
    ];

    protected $fillable = [
        'writer_id',
        'name',
        'genre'
    ];

    protected $with = [
        'writer'
    ];

    public function writer()
    {
        return $this->belongsTo(Writer::class);
    }
}
