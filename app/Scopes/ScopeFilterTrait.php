<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * Trait ScopeFilterTrait
 * @package App\Scopes
 *
 * @method Builder filter(array $request)
 */
trait ScopeFilterTrait
{
    /**
     * @param Builder $query
     * @param array   $request
     *
     * @return Builder
     */
    public function scopeFilter(Builder $query, array $request): Builder
    {
        /** @var Model $model */
        $model = $this;

        $columns = $model->getConnection()
            ->getSchemaBuilder()
            ->getColumnListing($model->getTable());

        foreach ($request as $column => $value) {
            if (in_array($column, $columns)) {
                switch (true) {
                    case is_null($value):
                        $query->whereNull($column);
                        break;
                    case Str::startsWith($value, $operator = 'min:'):
                        $query->where($column, '>=', Str::after($value, $operator));
                        break;
                    case Str::startsWith($value, $operator = 'max:'):
                        $query->where($column, '<=', Str::after($value, $operator));
                        break;
                    case Str::startsWith($value, $operator = 'search:'):
                        $query->where($column, 'like', '%' . Str::after($value, $operator) . '%');
                        break;
                    default:
                        $query->where($column, $value);
                }
            }
        }

        if (isset($request[ 'sort' ])) {
            [ $column, $direction ] = explode(':', $request[ 'sort' ], 2);

            if (in_array($column, $columns) && in_array($direction, [ 'asc', 'desc' ])) {
                $query->orderBy($column, $direction);
            }
        }

        return $query;
    }
}
