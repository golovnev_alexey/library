<?php

namespace App\Responses;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Response;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class ApiResponse
 * @package App\Responses
 */
class ApiResponse
{
    /**
     * @param     $data
     * @param int $status
     *
     * @return Response
     */
    public static function json($data, $status = 200): Response
    {
        switch (true) {
            case is_a($data, Arrayable::class);
                $data = $data->toArray();
                break;
            case ! is_array($data):
                $data = [ $data ];
                break;
        }

        $response = new Response($data, $status);

        return $response->header('Content-Type', 'application/json');
    }
}
