<?php

use App\Http\Controllers\BookController;
use App\Http\Controllers\ReaderBookController;
use App\Http\Controllers\ReaderController;
use App\Http\Controllers\WriterController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::prefix('books')->group(function () {
    Route::get('/', [ BookController::class, 'index' ]);
    Route::get('/{id}', [ BookController::class, 'show' ]);
    Route::post('/', [ BookController::class, 'store' ]);
    Route::put('/{id}', [ BookController::class, 'update' ]);
    Route::delete('/{id}', [ BookController::class, 'destroy' ]);
});

Route::prefix('readers')->group(function () {
    Route::get('/', [ ReaderController::class, 'index' ]);
    Route::get('/{id}', [ ReaderController::class, 'show' ]);
    Route::post('/', [ ReaderController::class, 'store' ]);
    Route::put('/{id}', [ ReaderController::class, 'update' ]);
    Route::delete('/{id}', [ ReaderController::class, 'destroy' ]);
});

Route::prefix('writers')->group(function () {
    Route::get('/', [ WriterController::class, 'index' ]);
    Route::get('/{id}', [ WriterController::class, 'show' ]);
    Route::post('/', [ WriterController::class, 'store' ]);
    Route::put('/{id}', [ WriterController::class, 'update' ]);
    Route::delete('/{id}', [ WriterController::class, 'destroy' ]);
});

Route::prefix('reader-books')->group(function () {
    Route::get('/', [ ReaderBookController::class, 'index' ]);
    Route::get('/{id}', [ ReaderBookController::class, 'show' ]);
    Route::post('/', [ ReaderBookController::class, 'store' ]);
    Route::put('/{id}', [ ReaderBookController::class, 'update' ]);
    Route::delete('/{id}', [ ReaderBookController::class, 'destroy' ]);
});
