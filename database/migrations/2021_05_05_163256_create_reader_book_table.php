<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReaderBookTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reader_book', function (Blueprint $table) {
            $table->id();
            $table->foreignId('reader_id');
            $table->foreignId('book_id');
            $table->date('start_at');
            $table->date('end_at')->nullable();
            $table->timestamps();

            $table->foreign('reader_id')->on('readers')->references('id')->cascadeOnDelete()->cascadeOnUpdate();
            $table->foreign('book_id')->on('books')->references('id')->cascadeOnDelete()->cascadeOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reader_book');
    }
}
